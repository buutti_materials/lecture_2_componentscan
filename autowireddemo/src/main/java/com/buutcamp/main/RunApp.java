package com.buutcamp.main;

import com.buutcamp.animals.Animal;
import com.buutcamp.animals.Dog;
import com.buutcamp.configuration.AppConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class RunApp {

    public RunApp() {
        ApplicationContext appCtx = new AnnotationConfigApplicationContext(AppConfig.class);

        Animal animal = appCtx.getBean(Dog.class);

            /*Could show downcasting here*/

        //Dog dog = (Dog) animal;
        //((Dog) animal).setName("Doggie");
        //System.out.println(((Dog) animal).getName());

        ((AnnotationConfigApplicationContext) appCtx).close();
    }
}
