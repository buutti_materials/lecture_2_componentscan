package com.buutcamp.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.buutcamp")
//For xml use --> <context:component-scan base-package="com.buutcamp"/>
public class AppConfig {


}
