package com.buutcamp.animals;


import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal {

    private String name;

    public int hasHowManyLegs() {
        return 0;
    }

    public boolean isWildAnimal() {
        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
