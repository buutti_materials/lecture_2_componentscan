package com.buutcamp.animals;

public interface Animal {

    int hasHowManyLegs();

    boolean isWildAnimal();
}
